# SIE-RSS-CLIENT

## Getting Started

This is a POC to test an RSS call from a JAVA API

### Prerequisites

1. Maven 3.x
2. Java 7
3. Network Access 

### Installing and launching

1. ``cd ${WORKSPACE}``

2. ``mvn clean install``

3. ``cd target/``

3. ``java -jar rss-client-jar-with-dependencies.jar ${RSS_URL} ${OUTPUT_FILE_PATH}``

