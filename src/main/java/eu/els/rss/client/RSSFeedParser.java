package eu.els.rss.client;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
public class RSSFeedParser {

    private final URL url;

    public RSSFeedParser(String url) {
        try {
            log.info("Given URL is the following {}", url);
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public void readFeed(String output) {
        try (InputStream is = this.call()) {
            this.parseAndSave(is, output);
        } catch (IOException e) {
            log.info("Error occurred. Root cause is", e);
            throw new RuntimeException(e);
        }
    }

    private InputStream call() throws IOException {
        log.info("RSS call ...");
        try {
            InputStream is = url.openStream();
            log.info("RSS call is successful");
            return is;
        } catch (IOException e) {
            log.error("Error occurred when parsing RSS response.");
            throw e;
        }
    }

    private void parseAndSave(InputStream is, String output) throws IOException {
        log.info("Trying to parseAndSave the response content ...");
        try (OutputStream os = new FileOutputStream(new File(output))) {
            IOUtils.copy(is, os);
        } catch (IOException e) {
            log.error("Error occurred when parsing RSS response.");
            throw e;
        }
    }

}
