package eu.els.rss.client;

public class RSSClient {

    public static void main(String... args) {
        validateArgs(args);

        String url = args[0];
        String outputPath = args[1];

        RSSFeedParser parser = new RSSFeedParser(url);
        parser.readFeed(outputPath);
    }

    private static void validateArgs(String... args) {
        if (args != null && args.length >= 2) {
            return;
        }
        throw new IllegalArgumentException("Invalid program arguments. 1. ${URL}, 2. ${OutputFile}");
    }

}
